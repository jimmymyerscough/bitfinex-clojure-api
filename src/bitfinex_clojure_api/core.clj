(ns bitfinex-clojure-api.core
  (:require [clj-http.client :as http]
            [clojure.data.json :as json]
            [base64-clj.core :as b64]
            [pandect.algo.sha384 :refer :all]
            [clojure.java.io :as io]
            [clojure.data.csv :as csv]
            [clojure.tools.logging :as log])
  (:gen-class))


(def api_url "https://api.bitfinex.com")
(def api_key "NOT_SET")
(def api_secret "NOT_SET")

(defn auth [key secret]
  (def api_secret secret)
  (def api_key key))

(defn post [url payload]
  (log/debug "Loading " url)
  (if (or (= "NOT_SET" api_secret)(= "NOT_SET" api_key)) (throw (Exception. "Bitfinex Secret/Key not set. Call (auth <key> <secret>) first")))  
  
  (let [full_payload (conj payload {"request" url, "nonce" (str (* 1000 (System/currentTimeMillis)))})        
        json_payload (json/write-str full_payload)
        payload_base64 (b64/encode json_payload)
        signature (sha384-hmac payload_base64 api_secret)
        headers {"X-BFX-APIKEY" api_key, 
                 "X-BFX-PAYLOAD" payload_base64,
                 "X-BFX-SIGNATURE" signature}]
    (json/read-str (:body (http/post (str api_url url) {:headers headers})) :key-fn keyword)))

(defn public_get [url]
  (log/debug "Getting " url)
  (json/read-str (:body (http/get (str api_url url))) :key-fn keyword))

(defn lend_book [ccy limit_bids limit_asks]
  (public_get (str "/v1/lendbook/" ccy "?limit_bids=" limit_bids "&limit_asks=" limit_asks)))

(defn ticker [symbol]
  (public_get (str "/v1/pubticker/" symbol)))

(defn trades 
  ([symbol]
   (trades symbol nil nil))
  ([symbol number]
   (trades symbol number nil))
  ([symbol number since]
   (let [params (clojure.string/join "&" 
                  [(if (some? number) (str "limit_trades=" number) "")
                   (if (some? symbol) (str "timestamp=" since) "")])]
     (public_get (str "/v1/trades/" symbol "?" params)))))

(defn lend_book [symbol limit_bids limit_asks]
  (public_get (str "/v1/lendbook/" symbol "?limit_bids=" limit_bids "&limit_asks" limit_asks))) 

(defn lends
  ([symbol]
   (lends symbol nil nil))
  ([symbol number]
   (lends symbol number nil))
  ([symbol number since]
   (let [params (clojure.string/join "&" 
                  [(if (some? number) (str "limit_lends=" number) "")
                   (if (some? symbol) (str "timestamp=" since) "")])]
     (public_get (str "/v1/lends/" symbol "?" params)))))

(defn active_offers 
  ([]
   (active_offers nil))
  ([ccy]
   (let [offers (post "/v1/offers" {})] 
      (filter #(or (nil? ccy) (= ccy (:currency %))) offers))))

(defn active_lends 
  ([]
   (active_lends nil))
  ([ccy]
   (let [offers (active_offers ccy)] 
      (filter #(= "lend" (:direction %)) offers))))

(defn active_borrows 
  ([]
   (active_borrows nil))
  ([ccy]
   (let [offers (active_offers ccy)] 
      (filter #(= "borrow" (:direction %)) offers))))

(defn new_offer [ccy amount rate period direction]
  (post "/v1/offer/new" {"currency" ccy, "amount" amount, "rate" rate, "period" period, "direction" direction}))

(defn cancel_offer [id]
  (post "/v1/offer/cancel" {:offer_id id}))

(defn wallet_balances []
  (post "/v1/balances" {}))

(defn v2_trades [symbol limit since]
  (public_get (str "/v2/trades/" symbol "/hist?limit=" limit "&start=" since "&sort=1")))

(defn wallet_balance [type currency]
  (first (filter #(and 
                       (= (clojure.string/lower-case currency) (:currency %))
                       (= (clojure.string/lower-case type) (:type %))) 
           (wallet_balances))))
