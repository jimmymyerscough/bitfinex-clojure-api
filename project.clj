(defproject bitfinex-clojure-api "0.0.1-SNAPSHOT"
  :description "Clojure wrapper for Bitfinex API 2.0"
  :dependencies [[org.clojure/clojure "1.9.0"] 
                 [clj-http "3.10.0"]
                 [org.clojure/data.json "0.2.6"]
                 [base64-clj "0.1.1"]
                 [pandect "0.6.1"]
                 [org.clojure/data.csv "0.1.4"]
                 [org.clojure/tools.logging "0.5.0"]
                 [log4j/log4j "1.2.17"]]
  :aot [bitfinex-clojure-api.core]
  :main bitfinex-clojure-api.core)
